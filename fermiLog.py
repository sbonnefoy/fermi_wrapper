class Logger:

   def __init__(self, stream, fname):
       self.stream = stream
       self.file = open(fname,'w')  # File where you need to keep the logs

   def write(self, data):
       self.stream.write(data)
       self.stream.flush()      # unbuffered
       self.file.write(data)    # Write the data of stdout here to a text file as well
       self.file.flush()        # unbuffered
