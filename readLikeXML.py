def header(model):
    fileWrite = open(str(model),'w')
    fileWrite.write('<?xml version="1.0" ?>\n')
    fileWrite.write('<source_library title="source library">\n')
    fileWrite.write('<!-- Point Sources -->\n')
    fileWrite.close()

def tail(model):
    fileWrite = open(str(model),'a')
    fileWrite.write('</source_library>')

def countLine(model):
    file = open(model,'r')
    eof="default"
    line=0
    while eof != "":
        eof = file.readline()
        line = line +1
    return line

def modelOut(src,pivot):
    return str(src)+"_model_BinnedPL_"+str(pivot)+"_GeV.xml"

#################################################################
#
#      Beginning of the class to create the tuned source model
#
#################################################################

class readLikeXML:

    def __init__(self, preFactor, pivot, srcName, srcFermiName, model, fl, fA, modelOutput, TPL):
        self.preFactor = preFactor
        self.pivot=pivot
        self.SIF=srcFermiName
        self.SI=srcName
        self.model=model
        self.factorLimit=fl
        self.fitAlpha=fA
        self.modelOutput=modelOutput
        self.TPL = TPL

    def setFitAlpha(self, fitAlpha):
        if fitAlpha:
            self.fitAlpha=1
        else:
            self.fitAlpha=0

    
    def fillSIBinnedPL(self, likelihood):
        """filling the xml file for the source of interest
        The binned power law creates a power law with index -2 to make a fit in each energy band.
        The pivot energy is the energy where will be plotted the spectral point"""
        
        header(self.modelOutput)
        nbline=countLine(self.model)
        flagSource=0
        flagSpatial=0
        flagWrite=0
        flagSourceWritten=0
        fileWrite = open(str(self.modelOutput),'a')
        sourceInterest = self.SI
        fileRead = open(self.model,'r')
        i=0
        while i<nbline+1:
            line = fileRead.readline()
            sourceName=""
            if "<source name" in line:
                startName=line.find('name=\"')+6                           # Getting the source model name
                stopName=line.find(' type=', startName)-1
                sourceName = line[startName:stopName]
                
                if "_" in sourceName: 
                    sourceName = sourceName.replace("_","")               

                              

                if sourceName == self.SIF:                              #if the source is not present in the SOI list, then it will be computed
                    sourceLine = line
                    flagSource=1
                    if likelihood==1:
                        fileWrite.write(line)
                    bufferModel=""
           
        #        if sourceName == "NebSync" and self.TPL:                              #if the source is not present in the SOI list, then it will be computed
        #            sourceLine = line
        #            flagSource=1
        #            bufferModel=""

            if likelihood == 2 and flagSource==1:
                flagSpatial=1                  


            # Writting power law for the source of interest in the new model
            #

            if flagSource ==1 and flagSpatial==0 and likelihood==1 and flagSourceWritten==0:

                #Extracting the scale from the prefactor value
                #
                
                for scale in range(1,30):
                    if self.preFactor*10**scale >=1:
                        break
                    
                preFactor = self.preFactor*10**scale
                prefMin = preFactor*1e-7
                prefMax = preFactor*1e+7

                fileWrite.write("<spectrum type=\"PowerLaw\"> \n")
                fileWrite.write("\t \t<parameter free=\"1\" max=\""+ str(prefMax)+"\" min=\""+str(prefMin)+"\" name=\"Prefactor\" scale=\"1e-"+str(scale)+"\" value=\""+str(preFactor)+"\"/>\n")
                if self.fitAlpha == 1:
                    fileWrite.write("\t \t<parameter free=\"1\" max=\"0.0\" min=\"-5.0\" name=\"Index\" scale=\"1.0\" value=\"-2\"/>\n")
                else:
                    fileWrite.write("\t \t<parameter free=\"0\" max=\"0.0\" min=\"-5.0\" name=\"Index\" scale=\"1.0\" value=\"-2\"/>\n")
                fileWrite.write("\t \t<parameter free=\"0\" max=\"5e5\" min=\"30\" name=\"Scale\" scale=\"1.0\" value=\""+str(self.pivot)+"\"/>\n")
                fileWrite.write(" </spectrum> \n")
                flagSourceWritten=1

            # Writting power law for the source of interest in the new model
            # in case we have a broken power law

            if "NebSync" in sourceName and self.TPL:
                prefMin = self.preFactor*1e-2
                prefMax = self.preFactor*1e+2
                fileWrite.write("<source name=\""+str(self.SI)+"\" type=\"PointSource\">\n")
                fileWrite.write("<spectrum type=\"PowerLaw\"> \n")
                fileWrite.write("\t \t<parameter free=\"1\" max=\""+ str(prefMax)+"\" min=\""+str(prefMin)+"\" name=\"Prefactor\" scale=\"1\" value=\""+str(self.preFactor)+"\"/>\n")
                if self.fitAlpha == 1:
                    fileWrite.write("\t \t<parameter free=\"1\" max=\"0.0\" min=\"-5.0\" name=\"Index\" scale=\"1.0\" value=\"-2\"/>\n")
                else:
                    fileWrite.write("\t \t<parameter free=\"0\" max=\"0.0\" min=\"-5.0\" name=\"Index\" scale=\"1.0\" value=\"-2\"/>\n")
                    fileWrite.write("\t \t<parameter free=\"0\" max=\"5e5\" min=\"30\" name=\"Scale\" scale=\"1.0\" value=\""+str(self.pivot)+"\"/>\n")
                    fileWrite.write(" </spectrum> \n")
                    flagSourceWritten=1
                        
         

            if likelihood ==1 and flagSource==1 and "spatialModel" in line:
                flagSpatial=1
              

            if likelihood==1 and flagSource==1 and flagSpatial==1:
                fileWrite.write(line)
                if "</source>" in line :
                    flagSpatial=0
                    flagSource=0
                    flagSourceWritten=0

            if likelihood ==2 and flagSource==1 and flagSpatial==1:
                fileWrite.write(line)
                if "</source>" in line :
                    flagSource=0
                    flagSpatial=0
           
            i+=1
           
    def fillBkgSrc(self, scaleFactor):
        """Filling the source model file with the background sources"""
    
        
        fileWrite = open(str(self.modelOutput),'a')
        nbline=countLine(self.model)
        flagSource=0
        i=0
        numberFree=0
        fileRead = open(self.model,'r')
        scale=0
        bufferModel=""
        flagDiffuse = 0
        sourceName = ""
        while i<nbline+1:
            
            line = fileRead.readline()

            

            if "<source name" in line and flagSource==0:
                startName=line.find('name=\"')+6                           # Getting the source model name
                stopName=line.find(' type=', startName)-1
                sourceName = line[startName:stopName]
                bufferModel=""
             

            if "gll" in sourceName:
                flagDiffuse = 1   
                
                #if "_" in sourceName: 
                    #sourceName = sourceName.replace("_","")               

                             

            if sourceName not in self.SIF:                              #if the source is not present in the SOI list, then it will be computed
                sourceLine = line
                flagSource=1
                    


            if "Prefactor" in line or "norm" in line or "Normalization" in line :
                
                startScale=line.find('scale=\"')+7
                stopScale=line.find(' value=', startScale)-1
                scale = float(line[startScale:stopScale])
              
                startValue=line.find('value=\"')+7
                stopValue=line.find('/>', startValue)-2
                value = float(line[startValue:stopValue])
                line = line.replace(str(value), str(value*scaleFactor))
                
                             
	    if "free=\"1\"" in line:
	        numberFree+=1
	
            if flagDiffuse == 0:
                line = line.replace('free="1"','free="0"')
 
            if flagSource==1:
                bufferModel+=line
                    
            
            if "</source>" in line and flagSource==1 :
                flagSource=0
                if scale >= self.factorLimit and self.TPL and "NebIC" not in sourceName and "NebSync" not in sourceName:
                    fileWrite.write(bufferModel)
                if scale >= self.factorLimit and not self.TPL:
                    fileWrite.write(bufferModel)
                                        
            i = i+1

        fileRead.close() 
        fileWrite.close()
        tail(self.modelOutput)
        print "Found ", numberFree, " free parameters"


       
    def scaleModel(self, scaleFactor):

        fileWrite = open(str(self.modelOutput),'w')
        nbline=countLine(self.model)
        flagSource=0
        i=0
        fileRead = open(self.model,'r')
        scale=0
        flagSpectrum=0
        bufferModel=""
        sourceName=""
        while i<nbline+1:
            line = fileRead.readline()
            if "<spectrum" in line:
                flagSpectrum=1
            if "</spectrum>" in line:
                flagSpectrum = 0

            if "<source name" in line:
                startName=line.find('name=\"')+6                           # Getting the source model name
                stopName=line.find(' type=', startName)-1
                sourceName = line[startName:stopName]

            
            if self.TPL and "NebIC" not in sourceName and "NebSync" not in sourceName and flagSpectrum:
                if "Prefactor" in line or "norm" in line or "Normalization" in line :
                    startValue=line.find('value=\"')+7
                    stopValue=line.find('/>', startValue)-1
                    value = float(line[startValue:stopValue])
                    line = line.replace(str(value), str(value*scaleFactor))
                    
            if not self.TPL and self.SI not in sourceName and flagSpectrum:
                if "Prefactor" in line or "norm" in line or "Normalization" in line :
                    startValue=line.find('value=\"')+7
                    stopValue=line.find('/>', startValue)-1
                    value = float(line[startValue:stopValue])
                    line = line.replace(str(value), str(value*scaleFactor))
            i+=1
            fileWrite.write(line)

        fileRead.close() 
        fileWrite.close()
       

    def getPrefactorScale(self):
        '''
        Method get the scale of the prefactor. So far
        this method would be working for exponential and super-exponential
        cut-off
        '''

        nbline=countLine(self.model)
        sourceInterest = self.SI
        fileRead = open(self.model,'r')
        i=0
        flagSource = 0 
        scalePrefactor=0
        while i<nbline+1:
            line = fileRead.readline()
            sourceName=""
            if "<source" and  "name" in line and sourceInterest in line:
                flagSource =1
            if (flagSource==1 and "norm" in line) or (flagSource==1 and "Prefactor" in line):
                startName=line.find('scale=\"')+7                           # Getting the source model name
                stopName=line.find(' value=', startName)-1
                scalePrefactor = line[startName:stopName]
                print "The prefactor of the source ", sourceInterest, " is ", float(scalePrefactor)
                break
        
        return scalePrefactor
