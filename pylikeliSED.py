from math import sin,cos,pi,log,exp,log10,sqrt
import numpy as num
import pyfits
from ROOT import TVector, TCanvas, TGraph, TGraphErrors, TGraphAsymmErrors, TF1
import pyLikelihood as pyLike
from BinnedAnalysis import *

########## Settings ########## 
SOI  = '_crabP1' #Source Of Interest (for which spectral points are produced)
ftol = 0.001 #tolerance to start likelihood fit -> if ftol does not work trying fit with ftol/10 and ftol*10
fix  = 0 #If fix==1 all sources except the SOI will be fixed
ind  = 2 #Index of the power law for the pulsar (normally fixed to ind value). If ind==0 -> index is thawed

source_maps = 'CrabNebula_scrmap_0.983_1.026.fits'
mode        = 'simon'
output      = 'simon'

emins=(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14)
emaxs=(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15)

input_xml_SED    ='crabP1_model_like.xml'    #model in which the pulsar is moddeled with a power law
input_xml_global ='crabP1_model.xml' #global model that was used to obtain the spectral parameters -> is needed to calculate the weighted energy center and for plotting
exposure_cube    ='CrabNebula_ltcube_0.983_1.026.fits'
bd_exposure_map  ='CrabNebula_expcube_0.983_1.026.fits'
IRFS             ='P7SOURCE_V6'


########## Functions ##########
### Get parameter from a spectral model (copied from likeSED)
def getParamIndx(fit,name,NAME):
	ID=-1
	spec=fit[name].funcs['Spectrum']
	for indx, parName in zip(spec._parIds, spec.paramNames):
		if(parName==NAME):
			ID = indx
	if(ID==-1):
		print 'Parameter %s not found for source %s in file %s.' %(NAME,name,fit.srcModel)
	return ID

### Gives angular distance to SOI
def angsep(LIKE,source):
        try:
            RA=LIKE.model.srcs[SOI].src.getSrcFuncs()['Position'].getParamValue('RA')
            DEC=LIKE.model.srcs[SOI].src.getSrcFuncs()['Position'].getParamValue('DEC')
            ra=LIKE.model.srcs[source].src.getSrcFuncs()['Position'].getParamValue('RA')
            dec=LIKE.model.srcs[source].src.getSrcFuncs()['Position'].getParamValue('DEC')
        
            RA*=pi/180.
            DEC*=pi/180.
            ra*=pi/180.
            dec*=pi/180.
            cossep=cos(pi/2.-DEC)*cos(pi/2.-dec)+sin(pi/2.-DEC)*sin(pi/2.-dec)*cos(RA-ra)
            sep=num.arccos(cossep)*180./pi

	    return sep
	except:
	    if source=='Vela X':
	        return 20 #keep Vela X free, while Galactic and Iso will be fixed. Or not.
	    else:
		return 20    

### Definition of a PLCuttoff function through ROOT
def PLCutoff(glLIKE):
        P=getParamIndx(glLIKE,SOI,'Prefactor')
        S=getParamIndx(glLIKE,SOI,'Scale')
        I1=getParamIndx(glLIKE,SOI,'Index1')
        I2=getParamIndx(glLIKE,SOI,'Index2')
        C=getParamIndx(glLIKE,SOI,'Cutoff')
        func=TF1("f", "[0]*TMath::Power(x/[1],[2])*TMath::Exp(-1*TMath::Power(x/[3],[4]))",100,100000);
        func.SetParameter(0,glLIKE[P].value()*glLIKE[P].getScale())
	func.SetParameter(1,glLIKE[S].value()*glLIKE[S].getScale())
	func.SetParameter(2,glLIKE[I1].value()*glLIKE[I1].getScale())
	func.SetParameter(3,glLIKE[C].value()*glLIKE[C].getScale())
	func.SetParameter(4,glLIKE[I2].value()*glLIKE[I2].getScale())
        return func

def BPL(glLIKE):
        P=getParamIndx(glLIKE,SOI,'Prefactor')
        S=getParamIndx(glLIKE,SOI,'Scale')
        I1=getParamIndx(glLIKE,SOI,'Index1')
        I2=getParamIndx(glLIKE,SOI,'Index2')
        Eb=getParamIndx(glLIKE,SOI,'BreakValue')
	Beta=getParamIndx(glLIKE,SOI, 'Beta')
        func=TF1("f", "[0]*TMath::Power(x/[1],[2])*TMath::Power(1+TMath::Power(x/[3],([2]-[4])/[5]),-[5])",100,100000);
        func.SetParameter(0,glLIKE[P].value()*glLIKE[P].getScale())
	func.SetParameter(1,glLIKE[S].value()*glLIKE[S].getScale())
	func.SetParameter(2,glLIKE[I1].value()*glLIKE[I1].getScale())
	func.SetParameter(3,glLIKE[Eb].value()*glLIKE[Eb].getScale())
	func.SetParameter(4,glLIKE[I2].value()*glLIKE[I2].getScale())
	func.SetParameter(5,glLIKE[Beta].value()*glLIKE[Beta].getScale())	
        return func

### Definition of a PLCutoff + PowerLaw function through ROOT
def PLCutoff_PL(glLIKE):
        P=getParamIndx(glLIKE,SOI,'Prefactor')
        S=getParamIndx(glLIKE,SOI,'Scale')
        I1=getParamIndx(glLIKE,SOI,'Index1')
        I2=getParamIndx(glLIKE,SOI,'Index2')
        C=getParamIndx(glLIKE,SOI,'Cutoff')
	PLP=getParamIndx(glLIKE,SOII,'Prefactor')
        PLS=getParamIndx(glLIKE,SOII,'Scale')
        PLI=getParamIndx(glLIKE,SOII,'Index')
        func=TF1("f", "[0]*TMath::Power(x/[1],[2])*TMath::Exp(-1*TMath::Power(x/[3],[4]))+[5]*TMath::Power(x/[6],[7])",100,100000);
        func.SetParameters(glLIKE[P].value()*glLIKE[P].getScale(),glLIKE[S].value()*glLIKE[S].getScale(),glLIKE[I1].value()*glLIKE[I1].getScale(),glLIKE[C].value()*glLIKE[C].getScale(),glLIKE[I2].value()*glLIKE[I2].getScale(),glLIKE[PLP].value()*glLIKE[PLP].getScale(),glLIKE[PLS].value()*glLIKE[PLS].getScale(),glLIKE[PLI].value()*glLIKE[PLI].getScale())
	#func.SetParameters(1e-09,1000,-3.)
        return func

### Giving a weighted energy center
def weightedEcenter(FUNCTION,EMIN,EMAX):
	steps=(EMAX-EMIN)/100.
	EN=EMIN
	FSUM=0
	ECENT=0
	while EN < EMAX:
		FUNC=FUNCTION.Eval(EN)
		FSUM+=FUNC
		ECENT+=FUNC*EN
		EN+=steps
	return ECENT/FSUM


########## Likelihood Fit ##########
### For converting eV into erg, for plotting in the end
erg=1.602e-06 # 1MeV is 1.602e-6 erg

### Lists to record the values obtained by running likelihood
energy=[]
energy_err_high=[]
energy_err_low=[]
specpoints=[]
specpoints_err=[]
ts=[]
RetCode=[]

### Likelihood object
myobs=BinnedObs(srcMaps=source_maps,expCube=exposure_cube,binnedExpMap=bd_exposure_map,irfs=IRFS)

### Defining BinnedAnalysis object for global fit and its function -> weighted Ecenter and plot
global_like=BinnedAnalysis(myobs,input_xml_global,'NEWMINUIT')
function=BPL(global_like) #  <------------------------------------------------------------------------ Define global function here!!!

### Number of Energybands
f=pyfits.open(bd_exposure_map)
NumBin=len(f[1].data)


### Going through every Energyband
for i,j in zip(emins,emaxs):   #range(NumBin-10)

    band_like=BinnedAnalysis(myobs,input_xml_SED,'NEWMINUIT')
    band_likeobj=pyLike.NewMinuit(band_like.logLike)

    emin=f[1].data.field(0)[i]
    emax=f[1].data.field(0)[j]
    ecent=sqrt(emin*emax) # center in log space

    #weighted_ecent=weightedEcenter(function,emin,emax)
 
    band_like.setEnergyRange(emin+1,emax-1)

    ### Preparing parameters for all sources
    if fix==1:
        print 'Fixing all sources except %s' %SOI
        for source in band_like.sourceNames():
            if source!=SOI:
                for name in band_like.model[source].funcs['Spectrum'].paramNames:
                    param=getParamIndx(band_like,source,name)
                    band_like.freeze(param)
                
#    print 'Sources outside %f degrees will be frozen!' %radi
#    for source in band_like.sourceNames():
#	Sep=angsep(band_like,source)
#        if Sep > radi:
#            for name in band_like.model[source].funcs['Spectrum'].paramNames:
#                    param=getParamIndx(band_like,source,name)
#		    band_like.freeze(param)
#            #print 'Source %s %f degrees away -> freezed!' %(source,Sep)
#	else:
#            for name in band_like.model[source].funcs['Spectrum'].paramNames:
#		param=getParamIndx(band_like,source,name)    
#	        if name=='Prefactor' or name=='norm' or name=='Integral' or name=='Normalization':
#		    band_like.thaw(param)
#		else:
#		    band_like.freeze(param)	
#	    #print 'Source %s %f degrees away -> Normalization thawed!' %(source,Sep) 
#    print 'Done!'    

    ### Preparing parameters for the Pulsar (PowerLaw)
    scale=getParamIndx(band_like,SOI,'Scale')
    band_like[scale].setBounds(20,5e5)
    band_like[scale].setScale(1)
    #band_like[scale]=weighted_ecent
    band_like[scale]=ecent
    band_like.freeze(scale)
    #print 'Scale (weighted_ecent): %f' %weighted_ecent
    print 'Scale (not weighted ecent): %f' %ecent    

    pref=getParamIndx(band_like,SOI,'Prefactor') 
    band_like[pref].setBounds(1e-02,1e02)
    band_like[pref].setScale(10**int(log10(function.Eval(ecent))))
    band_like[pref]=function.Eval(ecent)/band_like[pref].getScale()
    band_like.thaw(pref)
    print 'Prefactor: %f' %band_like[pref].value()    

    index=getParamIndx(band_like,SOI,'Index')
    if ind!=0:
        band_like.freeze(index)
        band_like[index]=ind
    else:
	band_like.thaw(index)
	band_like[index]=2.5
    band_like[index].setBounds(1.5,4.0)
    band_like[index].setScale(-1)
    print 'Index: %f' %band_like[index].value()        

    #print band_like.model

    ### Do the actual fit
    try:
        print 'Starting likelihood fit with tolerance: %f' %ftol 
        band_like.fit(tol=ftol,covar=True,optObject=band_likeobj)
        fail=0
    except:
        try:
            print 'Starting likelihood fit with tolerance: %f' %(ftol/10) 
            band_like.fit(tol=ftol/10,covar=True,optObject=band_likeobj)
            fail=0
        except:
            try:
                print 'Starting likelihood fit with tolerance: %f' %(ftol*10)                 
                band_like.fit(tol=ftol*10,covar=True,optObject=band_likeobj)
                fail=0
            except:    
                print 'No convergence achieved in Energyband %i (%f - %f)!' %(i,emin,emax)
                fail=1

    print '########## log(Likelihood) for Energyband %i (%f - %f):' %(i,emin,emax), band_like.logLike.value(), '##########'
    RetCode.append(band_likeobj.getRetCode())
    if band_likeobj.getRetCode()>0:
        print '!!!!!!!!!!!!!!!!!!!! getRetCode: %f (Fit did not converge) !!!!!!!!!!!!!!!!!!!! ' %band_likeobj.getRetCode()
    #print 'getRetCode: ', band_likeobj.getRetCode()

    ### Record values if likelihood converged
    if fail==0:
        print 'TS for %s in Energyband %i (%f - %f): ' %(SOI,i,emin,emax),band_like.Ts(SOI)
        #print 'Prefactor is %15.13f' %(band_like[pref].value()*band_like[pref].getScale()) 

        energy.append(ecent)
        energy_err_low.append(ecent-emin)
	energy_err_high.append(emax-ecent)
        specpoints.append(band_like[pref].value()*band_like[pref].getScale()*ecent*ecent*erg) # E*E*dN/dE Plot
        specpoints_err.append(band_like[pref].error()*band_like[pref].getScale()*ecent*ecent*erg)

        #energy.append(weighted_ecent)
        #energy_err_low.append(weighted_ecent-emin)
	#energy_err_high.append(emax-weighted_ecent)
        #specpoints.append(band_like[pref].value()*band_like[pref].getScale()*weighted_ecent*weighted_ecent*erg) # E*E*dN/dE Plot
        #specpoints_err.append(band_like[pref].error()*band_like[pref].getScale()*weighted_ecent*weighted_ecent*erg)
        ts.append(band_like.Ts(SOI))

    ### Deleting Likelihood Object in order to avoid memory issues
    del band_like
    del band_likeobj


########## Plot Section ##########
### Create TVectors for Plotting 
n=len(energy)
En=TVector(n)
En_err_high=TVector(n)
En_err_low=TVector(n)
Spec=TVector(n)
Spec_err=TVector(n)
TSvalues=TVector(n)

### Copying values to TVectors and print out a list of TS values, Spec-Points and RetCode for each energy band

OutputFile = open('./SP_%s.txt' %output, 'w')

print '\n\n########## Results ##########'
for i in range(n):
    print 'Energy band %i: %f - %f' %(i,energy[i]-energy_err_low[i],energy[i]+energy_err_high[i])
    print '  TS for %s:' %SOI,ts[i]
    print '  E :%f | E*E*dN/dE: (%f +/- %f) * 10^9' %(energy[i],specpoints[i]*1e09,specpoints_err[i]*1e09)
    print '  RetCode:',RetCode[i]

    En[i]=energy[i]
    En_err_high[i]=energy_err_high[i]
    En_err_low[i]=energy_err_low[i]
    Spec[i]=specpoints[i]
    Spec_err[i]=specpoints_err[i]
    TSvalues[i]=ts[i]
    
    OutputFile.write(str(energy[i])+' '+str(specpoints[i])+' '+str(energy_err_low[i])+' '+str(energy_err_high[i])+' '+str(specpoints_err[i])+' '+str(specpoints_err[i])+' '+str(ts[i])+'\n')   

OutputFile.close()

### Graph for SED
c1=TCanvas("c1","Spectral Points",1)
c1.SetLogx()
c1.SetLogy()

#Producing function values for the global likelihood fit function... Sometimes ROOT just behaves strange... 
points=100 #When creating a TGraph with the TF1 as argument, it doesnt plot the function from the beginning (100MeV)...
x=TVector(points)
y=TVector(points)
j=100. #function variable (E in MeV) for the global likelihood fit function
jmax=300000.
step=(jmax/j)**(1./points)
for i in range(points):
    x[i]=j
    y[i]=function.Eval(j)*j*j*erg #E*E dN/dE Plot in erg
    j*=step

graph_func=TGraph(x,y) #Maximum Likelihood Model
graph_func.GetXaxis().SetTitle("Energy (MeV)")
graph_func.GetXaxis().CenterTitle(True)
graph_func.GetYaxis().SetTitle("E^2 dN/dE (erg cm^-2 s^-1)")
graph_func.GetYaxis().CenterTitle(True)

graph_points=TGraphAsymmErrors(En,Spec,En_err_low,En_err_high,Spec_err,Spec_err) #Energy Band Fits

graph_func.Draw("AC")
graph_points.Draw("P*")

### Graph for TS values
c2=TCanvas("c2","TS Values",1)
c2.SetLogx()

graph_TS=TGraph(En,TSvalues)
graph_TS.GetXaxis().SetTitle("Energy (MeV)")
graph_TS.GetXaxis().CenterTitle(True)
graph_TS.GetYaxis().SetTitle("TS Value")
graph_TS.GetYaxis().CenterTitle(True)

graph_TS.Draw("ALP*")

raw_input("Press Enter to exit")
