from preAnalysis import *
from fermiBinnedLike import *
from fermiUnbinnedLike import *
from readLikeXML import *
from math import sqrt
import ROOT
from ROOT import TCanvas
from ROOT import TF1
from ROOT import TFile
import os.path
from makeXML import *
from array import *
from inputFermi import *
from termcolor import colored, cprint
from fermiLog import *

###############################################
#
#    Running the pre-analysis of Fermi data
#
###############################################
logfile="log.txt"
sys.stdout = Logger(sys.stdout, logfile) # This write both to file and console
sys.stderr = Logger(sys.stdout, logfile)

def preAnalysisBinned():
    preAn = preAnalysis(srcName, scfile, RA, DEC, ROI, EMIN, EMAX, EBIN, ZMAX, IRFS,PHASEMIN, PHASEMAX)

    cprint ("\n == Running gtselect == ", 'green')
    cprint ("No phase selection will be done here. \n", 'green')
    preAn.gtselect(0, pulsarMode)
    cprint ("\n == Running gtmktime == \n", 'green')
    preAn.gtmktime(pulsarMode)
    
    if pulsarMode: #For pulsar mode we have to phase the data
        
        cprint ("\n You are running a pulsar analysis, lucky you!!! ", 'green')
        if phaseSoft == "gtpphase":                    # if using tempo2, the events are phased before, if using gtpphase, phasing is done here. 
            cprint (" The events' phase will be computed using gtpphase! \n", 'green')
            preAn.gtpphase(EPHEMERIS, PSRNAME, pulsarMode, solarEph)
       
        else:
            cprint ("The events' phase was computed using tempo2. Make sure the phase has been computed!", 'green')
        
        cprint ( "\n == Running ftselect == \n", 'green')
        cprint ("The phase selection will be done here, according to:",'green')
        cprint (phaseCut, 'green')
      
        preAn.ftselect(phaseCut)
    
    
    cprint ( "\n == Running gtbin, computing the CMAP == \n", 'green')
    preAn.gtbin("CMAP")
    cprint ( "\n == Running gtbin, computing the CCUBE == \n", 'green')
    preAn.gtbin("CCUBE")
    cprint ( "\n == Running gtltcube == \n", 'green')
    preAn.gtltcube()
    cprint ( "\n == Running gtexpcube2, computing expcube == \n", 'green')
    preAn.gtexpcube2('_expcube_')
    cprint ( "\n == Running gtexpcube2, computing allsky == \n", 'green')
    preAn.gtexpcube2('_allsky_')
        
    if not os.path.isfile("./"+str(model)):
        cprint ( "\n ==== Making the source model from the Fermi catalog ==== \n", 'green')
        make = makeXML(model, galactic,  srcName+'_gti_'+PHASEMIN+'_'+PHASEMAX+'.fits', iso, catalog, 1)
        make.makeModel()
        
    else:
        cprint ( "\n ==== Model existing, we skip this step ==== \n", 'green')
            
    cprint ( "\n == Running gtsrcmodel == \n", 'green')
    preAn.gtsrcmaps(model)

def preAnalysisUnbinned():
    preAn = preAnalysis(srcName, scfile, RA, DEC, ROI, EMIN, EMAX, EBIN, ZMAX, IRFS,PHASEMIN, PHASEMAX)
    
    preAn.gtselect(0, pulsarMode)
    preAn.gtmktime(pulsarMode)

    if pulsarMode:
        preAn.gtpphase(EPHEMERIS, PSRNAME, pulsarMode)
        preAn.ftselect(phaseCut)
            
    preAn.gtltcube()
    preAn.gtexpmap()
 
    if not os.path.isfile("./"+str(model)):
        print "==== Making the source model from the Fermi catalog ===="
        make = makeXML(model, galactic,  srcName+'_gti_'+PHASEMIN+'_'+PHASEMAX+'.fits', iso, catalog, 1)
        make.makeModel()
        
    else:
        print "==== Model existing, we skip this step ===="


#########################################
#
#   Running the likelihood analysis
#
#########################################
            
            
def likelihoodFit1(analysisType):


    if analysisType==1 :
        
        # Setting the binning and the list where the values for the final graph will be stocked.
        #

        incremEnergy=(log10(EMAX)-log10(EMIN))/EBIN
        energyLow=EMIN
        energy=array("f",[])
        flux=array("f",[])
        E2=array("f",[])
        exl=array("f",[])
        exh=array("f",[])
        eyl=array("f",[])
        eyh=array("f",[])
        tsArray=array("f",[])
        
        TPL=0             # Set if the expected flux is a broken power, in order for the XML class to know
                          # that 2 inputs (PL) have to be handled.

        if specType=="2PL":
            TPL=1
        
        modelScale=readLikeXML(1, 1, srcName, srcNameFermi, model, factorLimit, fitAlpha, "ModelScaleFirstAnalysis.xml",TPL)
                                                          # This is tricky because there is no method to read it from fermi tools

        modelScale.scaleModel(phaseFactor) # For pulsar analysis, all the sources' flux has to be scaled according to the phase region.
        preFactorScale = modelScale.getPrefactorScale()   # reading the prefactor scale in the xml model.

        binAn=fermiBinnedAnalysis(srcName, srcNameFermi, scfile, srcName+'_gti_'+PHASEMIN+'_'+PHASEMAX+'.fits', \
                                      srcName+'_ltcube_'+PHASEMIN+'_'+PHASEMAX+'.fits',  \
                                      srcName+'_scrmap_'+PHASEMIN+'_'+PHASEMAX+'.fits',  \
                                      srcName+'_expcube_'+PHASEMIN+'_'+PHASEMAX+'.fits', \
                                      srcName+'_ccube_'+PHASEMIN+'_'+PHASEMAX+'.fits',   \
                                      IRFS, EBIN, EMIN, EMAX, "ModelScaleFirstAnalysis.xml", tol)
        
        print "Here we are..."

        # Definition of power law with exponential cut-off function
        #
        if specType=="2PL":
            cprint ("\n == SED fitted with a broken power law == \n", 'green')
            try:
                cprint ("\n Trying to fit... \n", 'green')
                preFactorSync, scaleSync, alphaSync, preFactorIC, scaleIC, alphaIC = binAn.likelihood1(optimizer \
                                                                                                           , EMIN, EMAX, 0, 1, analysisType, specType)
            except BaseException:
                cprint ("\n ################ Fit did not converge ! ####################### \n", 'red')
            
    
            # Defining the two power-law function and the sum of both
            # 
  
            funcSpec = TF1("flux", "([0]*TMath::Power(x/[1], -[2]) + [3]*TMath::Power(x/[4], -[5]))", EMIN, EMAX)
            funcSync = TF1("fluxSync", "([0]*TMath::Power(x/[1], -[2]) )", EMIN, EMAX)
            funcIC = TF1("fluxIC", "([0]*TMath::Power(x/[1], -[2]) )", EMIN, EMAX)
            
            
            funcSpec.SetParameter(0, preFactorSync)         # Getting the parameter for the spectrum
            funcSpec.SetParameter(1, scaleSync)
            funcSpec.SetParameter(2, alphaSync)
            funcSpec.SetParameter(3, preFactorIC)
            funcSpec.SetParameter(4, scaleIC)
            funcSpec.SetParameter(5, alphaIC)
      
            funcSync.SetParameter(0,preFactorSync)          # Getting the parameters for each power law
            funcSync.SetParameter(1,scaleSync)
            funcSync.SetParameter(2,alphaSync)
            funcIC.SetParameter(0,preFactorIC)
            funcIC.SetParameter(1,scaleIC)
            funcIC.SetParameter(2,alphaIC)

            
            # Definition of power law with exponential cut-off function
            #

        if specType=="ExpCutOff":
            cprint ("\n == SED fitted with an exponential cutoff == \n", 'green')
         
            try:
                cprint ("\n Trying to fit... \n", 'green')
                preFactor, preFactorScale, index, scale, Ebreak, P1, P2, P3 = binAn.likelihood1(optimizer \
                                                                                    , EMIN, EMAX, 0, 1, analysisType, specType)
            except BaseException:
                cprint ("\n ################ Fit did not converge ! ####################### \n", 'red')
           
            
            funcSpec=TF1("flux", "([0]*[1]*TMath::Power(x/[2],-[3])*exp(-(x-([4]/1000))/[5]))", EMIN, EMAX)
            funcSpec.SetParameter(0, preFactor)
            funcSpec.SetParameter(1, float(preFactorScale))
            funcSpec.SetParameter(2, scale)
            funcSpec.SetParameter(3, index)
            funcSpec.SetParameter(4, Ebreak)
            funcSpec.SetParameter(5, P1)
            funcSpec.SetParameter(6, P2)
            funcSpec.SetParameter(7, P3)

        
        if specType=="PLSExpCutOff":
            print "== Power law exponential cutoff =="
           
            try:
                print "Trying to fit..."
                preFactor, index1, cutoff, scale, index2 = binAn.likelihood1(optimizer, EMIN, EMAX, 0, 1, analysisType, specType)

            except BaseException:
                print "################ Fit did not converge ! #######################"
                
            
            funcSpec=TF1("flux", "([0]*[1]*TMath::Power(x/[2],-[3])*exp(-TMath::Power((x/[4]),[5])))", EMIN, EMAX)
                  
            funcSpec.SetParameter(0, preFactor)
            funcSpec.SetParameter(1, float(preFactorScale))
            #funcSpec.SetParameter(1, 1e-9)
            funcSpec.SetParameter(2, scale)
            funcSpec.SetParameter(3, index1)
            funcSpec.SetParameter(4, cutoff)
            funcSpec.SetParameter(5, index2)

####################################
        if specType=="LogPara":
            print "== Log Parabola =="
           
            try:
                print "Trying to fit..."
                norm, alpha, beta, Eb = binAn.likelihood1(optimizer, EMIN, EMAX, 0, 1, analysisType, specType)

            except BaseException:
                print "################ Fit did not converge ! #######################"
                
            
            funcSpec=TF1("flux", "[0]*[1]*TMath::Power(x/[4],-([2]+[3]*TMath::Log10(x/[4])))", EMIN, EMAX)
                  
            funcSpec.SetParameter(0, norm)
            funcSpec.SetParameter(1, float(preFactorScale))
            funcSpec.SetParameter(2, alpha)
            funcSpec.SetParameter(3, beta)
            funcSpec.SetParameter(4, Eb)

####################################            

        print "##################### Fitting each bin with a power law ######################"

        fitSucc=1
        while energyLow <= EMAX-1:
            ts=0
            if fitSucc:
                energyUp = energyLow*(10**(incremEnergy))
            else:
                #energyUp = energyLow*(10**(incremEnergy))*(10**(incremEnergy))
                energyUp = energyUp*(10**(incremEnergy))
                print "Doing the fit again with a widder range"
                print energyUp, EMAX
                if energyUp > EMAX:
                    print " Energy Max is out of range!!!!"
                    break

            pivot = sqrt(energyLow*energyUp)
            print "Computing the fit between", energyLow, " and ", energyUp, " GeV; pivot = ", pivot
           
          
           
       # Creating the model for the several sources
      
            preFactor=funcSpec.Eval(pivot)                                                                    
            print "Prefactor from eval: ", preFactor
            
          
            for preFactorScale in range(1,30):
                if preFactor*10**preFactorScale >=1:
                    print "wouhou"
                    break
                    
                else:
                    print "not yet"
                  
                    
            modelBinned = str(srcName)+"_model_FinalFit-"+str(EMIN)+"-"+str(EMAX)+"MeV.xml"
            ''' The model to be used for the fit in each bin is the one generated by the full likelihood fit in which we set all sources to free="0"'''

            like=readLikeXML(preFactor, pivot,srcName, srcNameFermi, modelBinned, factorLimit, fitAlpha, str(srcName)+"_model_BinnedPL_"+str(pivot)+"_GeV.xml", TPL)
                                                                                                                # Making the model with power for the selected bin
            like.fillSIBinnedPL(1)                                                                               # Filling source of interest with power law
            like.fillBkgSrc(1)                                                                         # Filling background sources
           
               
            binAn=fermiBinnedAnalysis(srcName, srcNameFermi, scfile, srcName+'_gti_'+PHASEMIN+'_'+PHASEMAX+'.fits', \
                                          srcName+'_ltcube_'+PHASEMIN+'_'+PHASEMAX+'.fits',  \
                                          srcName+'_scrmap_'+PHASEMIN+'_'+PHASEMAX+'.fits',  \
                                          srcName+'_expcube_'+PHASEMIN+'_'+PHASEMAX+'.fits', \
                                          srcName+'_ccube_'+PHASEMIN+'_'+PHASEMAX+'.fits',   \
                                          IRFS, EBIN, EMIN, EMAX, str(srcName)+"_model_BinnedPL_"+str(pivot)+"_GeV.xml", tol)
       
          
           
            error=0
            try:
                preFactorFit, scale, alpha, error, ts = binAn.likelihood1(optimizer, energyLow, \
                                                                              energyUp,0 ,0, analysisType, "PL")
                
            except BaseException:
               print "################ Fit did not converge ! #######################"
               
            #correction the result of the fit by the scale
            preFactorFit = preFactorFit*(10**(-preFactorScale))
            flux_buffer=preFactorFit*((pivot/scale)**(alpha))*(1e6)
            tsArray.append(ts)
               
            if ts >= tsLimit:                                       # As in MAGIC, the spectral points are plotted if the significance is > 2.
                
                energy.append(pivot*1e-3)                               
               
                flux.append((1./(float(PHASEMAX)-float(PHASEMIN)))*preFactorFit*(((pivot/scale)**(-2))*(1e6)))
                if sedType=="ergs" and scaleSED==1:
                    E2_buffer=(pivot*pivot*flux_buffer*(10**-12)*1.6)/(float(PHASEMAX)-float(PHASEMIN))          # -> convert energy from MeV to GeV
                if sedType=="ergs" and scaleSED==0:
                    E2_buffer=(pivot*pivot*flux_buffer*(10**-12)*1.6)      
                if sedType=="TeV" and scaleSED==1:
                    E2_buffer=(pivot*pivot*flux_buffer*(10**-12))/(float(PHASEMAX)-float(PHASEMIN))   
                if sedType=="TeV" and scaleSED==0:
                    E2_buffer=(pivot*pivot*flux_buffer*(10**-12))   
                    
                E2.append(E2_buffer)
               

           ## Computing error bars ##
               
                exl.append((pivot-energyLow)/1000)
                exh.append((energyUp-pivot)/1000)
               
                if sedType=="ergs" and scaleSED==1:
                    eyl.append(pivot*pivot*(error)*(1e-6)*1.6/(float(PHASEMAX)-float(PHASEMIN))*(10**(-preFactorScale)))
                    eyh.append(pivot*pivot*(error)*(1e-6)*1.6/(float(PHASEMAX)-float(PHASEMIN))*(10**(-preFactorScale)))
                if sedType=="ergs" and scaleSED==0:
                    eyl.append(pivot*pivot*(error)*(1e-6)*1.6*(10**(-preFactorScale)))
                    eyh.append(pivot*pivot*(error)*(1e-6)*1.6*(10**(-preFactorScale)))
                if sedType=="TeV" and scaleSED==1:
                    eyl.append(pivot*pivot*(error)*(1e-6)/(float(PHASEMAX)-float(PHASEMIN))*(10**(-preFactorScale)))
                    eyh.append(pivot*pivot*(error)*(1e-6)/(float(PHASEMAX)-float(PHASEMIN))*(10**(-preFactorScale)))
                if sedType=="TeV" and scaleSED==0:
                    eyl.append(pivot*pivot*(error)*(1e-6)*(10**(-preFactorScale)))
                    eyh.append(pivot*pivot*(error)*(1e-6)*(10**(-preFactorScale)))
                               

                print "*******************" 
                print "***** After fit; energy: ", pivot, ", prefactor fitted: ", preFactorFit, ", E2:", E2_buffer, ", TS: ", ts
                print "*******************" 
            
                energyLow = energyUp
                fitSucc=1
            
            if ts < tsLimit:                      
                fitSucc=0
                print "Significance", ts, "  below limit value, setting widder enery bin!"
           


        ## Creation of the SED plot
        print "Creating the SED plot"
        print "Energy: ", energy, ", Significance: ", tsArray
        print "test1"
        SED = TGraphAsymmErrors(len(exl), energy, E2, exl, exh, eyl, eyh)
        SED.SetName("SED")
        print "test2"
        SED.SetNameTitle("SED", "SED")
        print "test3"
        SED.GetXaxis().SetTitle("Energy [GeV]")
        print "test4"
        
        if sedType=="ergs":
            SED.GetYaxis().SetTitle("E^{2}xFlux[erg.cm^{-2}s^{1]}]")
        if sedType=="TeV":
            SED.GetYaxis().SetTitle("E^{2}xFlux[TeV.cm^{-2}s^{1]}]")
            print "test6"
        if specType=="2PL":                 #If the spectrum is a broken power law, both power law are plotted
            print "test7"
            SED.SetMarkerStyle(21)
            c1 = TCanvas("sed")
            c1.SetLogx()
            c1.SetLogy()
            SED.Draw("AP")

            funcSpecPlot = TF1("flux", "x*x*([0]*TMath::Power(1000*x/[1], -[2]) + [3]*TMath::Power(1000*x/[4], -[5]))", EMIN/1000, EMAX/1000)
            print "test8"
            funcSyncPlot = TF1("fluxSync", "x*x*([0]*TMath::Power(1000*x/[1], -[2]) )", EMIN/1000, EMAX/1000)
            print "test9"
            funcICPlot = TF1("fluxIC", "x*x*([0]*TMath::Power(1000*x/[1], -[2]) )", EMIN/1000, EMAX/1000)
            print "test10"
            if sedType=="ergs" and scaleSED==1:
                funcSpecPlot.SetParameter(0, 1.6*preFactorSync/(float(PHASEMAX)-float(PHASEMIN))) 
                funcSpecPlot.SetParameter(3, 1.6*preFactorIC/(float(PHASEMAX)-float(PHASEMIN)))
                funcSyncPlot.SetParameter(0,1.6*preFactorSync/(float(PHASEMAX)-float(PHASEMIN)))   
                funcICPlot.SetParameter(0,1.6*preFactorIC/(float(PHASEMAX)-float(PHASEMIN)))
            print "test11"
            if sedType=="ergs" and scaleSED==0:
                funcSpecPlot.SetParameter(0, 1.6*preFactorSync)  
                funcSpecPlot.SetParameter(3, 1.6*preFactorIC)
                funcSyncPlot.SetParameter(0,1.6*preFactorSync)
                funcICPlot.SetParameter(0,1.6*preFactorIC)
            print "test12"
            if sedType=="TeV"and scaleSED==1: 
                funcSpecPlot.SetParameter(0, preFactorSync/(float(PHASEMAX)-float(PHASEMIN)))
                funcSpecPlot.SetParameter(3, preFactorIC/(float(PHASEMAX)-float(PHASEMIN)))
                funcSyncPlot.SetParameter(0,preFactorSync/(float(PHASEMAX)-float(PHASEMIN)))
                funcICPlot.SetParameter(0,preFactorIC/(float(PHASEMAX)-float(PHASEMIN)))
            print "test13"
            if sedType=="TeV"and scaleSED==0: 
                funcSpecPlot.SetParameter(0, preFactorSync)
                funcSyncPlot.SetParameter(0,preFactorSync) 
                funcSpecPlot.SetParameter(3, preFactorIC)
                funcICPlot.SetParameter(0,preFactorIC)

            print "test13"
            funcSpecPlot.SetParameter(1, scaleSync)
            funcSpecPlot.SetParameter(2, alphaSync)
            funcSpecPlot.SetParameter(4, scaleIC)
            funcSpecPlot.SetParameter(5, alphaIC)             
            funcSyncPlot.SetParameter(1,scaleSync)
            funcSyncPlot.SetParameter(2,alphaSync)
            funcICPlot.SetParameter(1,scaleIC)
            funcICPlot.SetParameter(2,alphaIC)

            funcSpecPlot.Draw("SAME");
            funcSyncPlot.SetMarkerStyle(8)
            funcSyncPlot.SetLineColor(3)
            funcSyncPlot.Draw("SAME");
            funcICPlot.SetMarkerStyle(8)
            funcICPlot.SetLineColor(4)
            funcICPlot.Draw("SAME")
      
        if specType=="PLSExpCutOff":
            funcSpecPlot=TF1("flux", "x*x*([0]*[1]*TMath::Power(1000*x/[2],-[3])*exp(-TMath::Power((1000*x/[4]),[5])))", EMIN/1000, EMAX/1000)
#            funcSpecPlot=TF1("flux", "x*x*([0]*[1]*TMath::Power(1000*x/[2],-[3])*exp(-(1000*x/[4])))", EMIN/1000, EMAX/1000)
            
            if sedType=="ergs":      
                funcSpecPlot.SetParameter(0, 1.6*funcSpec.GetParameter(0))
            if sedType=="TeV":      
                funcSpecPlot.SetParameter(0, funcSpec.GetParameter(0))
                
            funcSpecPlot.SetParameter(1, funcSpec.GetParameter(1))
            funcSpecPlot.SetParameter(2, funcSpec.GetParameter(2))
            funcSpecPlot.SetParameter(3, funcSpec.GetParameter(3))
            funcSpecPlot.SetParameter(4, funcSpec.GetParameter(4))
            funcSpecPlot.SetParameter(5, funcSpec.GetParameter(5))
            

            SED.SetMarkerStyle(21)
            c1 = TCanvas("sed")
            c1.SetLogx()
            c1.SetLogy()
            SED.Draw("AP")
            funcSpecPlot.Draw("SAME")

            ######################################
        if specType=="LogPara":
            funcSpecPlot=TF1("flux", "x*x*[0]*[1]*TMath::Power(1000*x/[4],-([2]+[3]*TMath::Log10(1000*x/[4])))", EMIN/1000, EMAX/1000)
            
            if sedType=="ergs":      
                funcSpecPlot.SetParameter(0, 1.6*funcSpec.GetParameter(0))
            if sedType=="TeV":      
                funcSpecPlot.SetParameter(0, funcSpec.GetParameter(0))
                
            funcSpecPlot.SetParameter(1, funcSpec.GetParameter(1))
            funcSpecPlot.SetParameter(2, funcSpec.GetParameter(2))
            funcSpecPlot.SetParameter(3, funcSpec.GetParameter(3))
            funcSpecPlot.SetParameter(4, funcSpec.GetParameter(4))
            

            SED.SetMarkerStyle(21)
            c1 = TCanvas("sed")
            c1.SetLogx()
            c1.SetLogy()
            SED.Draw("AP")
            funcSpecPlot.Draw("SAME")

        if specType == "ExpCutOff":

            funcSpecPlot=TF1("flux", "x*x*(1e-6)*([0]*TMath::Power(x/[1],-[2])*exp(-(x-([3]/1000))/[4]))", EMIN/1000, EMAX/1000)
            if sedType=="ergs":    
                funcSpecPlot.SetParameter(0, 1.6*preFactor)
            if sedType=="TeV":    
                funcSpecPlot.SetParameter(0, preFactor)

            funcSpecPlot.SetParameter(1, scale)
            funcSpecPlot.SetParameter(2, index)
            funcSpecPlot.SetParameter(3, Ebreak)
            funcSpecPlot.SetParameter(4, P1)
            funcSpecPlot.SetParameter(5, P2)
            funcSpecPlot.SetParameter(6, P3)
            
            SED.SetMarkerStyle(21)
            c1 = TCanvas("sed")
            c1.SetLogx()
            c1.SetLogy()
            SED.Draw("AP")
            funcSpecPlot.Draw("SAME")
        
        ## Creation of the TS plot

        tsGraph = TGraphAsymmErrors(len(exl), energy, tsArray, exl, exh, eyl, eyh)
        tsGraph.SetName("TS")
        tsGraph.SetNameTitle("TS", "TS")
        tsGraph.GetXaxis().SetTitle("Energy [MeV]")
        tsGraph.GetYaxis().SetTitle("TS")
        tsGraph.SetMarkerStyle(21)
        c2 = TCanvas("ts")
        c2.SetLogx()
        c2.SetLogy()
        tsGraph.Draw("AP")

        ##Overall fit plot

        funcSpecPlot.Draw()

        ## Saving the plots in a root file
       
        c3=TCanvas("fluxCanvas")
        fileOutput = str(srcName)+"_"+str(EBIN)+"bins_"+str(EMIN)+"_"+str(EMAX)+"GeV_SED_fitAlpha"+str(fitAlpha)+"_Tol"+str(tol)+"_"+sedType+".root"
        f = TFile(fileOutput,"recreate");
        c1.Write()
        SED.Write()
        tsGraph.Write()
        funcSpecPlot.Write()
        f.Close()

        


    if analysisType==2:
        
        binAn=fermiBinnedAnalysis(srcName, "_"+srcNameFermi, scfile, srcName+'_gti_'+PHASEMIN+'_'+PHASEMAX+'.fits', \
                                      srcName+'_ltcube_'+PHASEMIN+'_'+PHASEMAX+'.fits',  \
                                      srcName+'_scrmap_'+PHASEMIN+'_'+PHASEMAX+'.fits',  \
                                      srcName+'_expcube_'+PHASEMIN+'_'+PHASEMAX+'.fits', \
                                      srcName+'_ccube_'+PHASEMIN+'_'+PHASEMAX+'.fits',   \
                                      IRFS, EBIN, EMIN, EMAX, model)

        binAn.likelihood1('MINUIT', EMIN, EMAX,0 ,1, analysisType, model)
        binAn.likelihood2('NewMinuit', 1)
        binAn.SED()


def unbinnedLike():
    unbinAn = fermiUnbinnedAnalysis(srcName, "_"+srcNameFermi, scfile, srcName+'_gti_'+PHASEMIN+'_'+PHASEMAX+'.fits', \
                                        srcName+'_ltcube_'+PHASEMIN+'_'+PHASEMAX+'.fits', \
                                        srcName+'_expmap_'+PHASEMIN+'_'+PHASEMAX+'.fits', \
                                        IRFS, EBIN, EMIN, EMAX, model)

    unbinAn.unBinnedlikeFit1('DRMNFB', EMIN, EMAX, 0, 1, 0)
    unbinAn.unBinnedlikeFit2('MINUIT', 1)
    unbinAn.unBinnedSED()


###########################################################################################
#
#           Running the analysis according to the analysis type set in the parameters 
#
###########################################################################################


if analysisType == 1:
    
    print "Running binned likelihood analysis"
    print "Each energy band will be fitted by an index 2 power law"

    if runPreAna:
        preAnalysisBinned()

    likelihoodFit1(analysisType)

if analysisType == 2:
     print "Running binned likelihood analysis"
     print "Two likelihood fits will be done, SED will be created by pytools"

     if runPreAna:
         preAnalysisBinned()
     
     likelihoodFit1(analysisType)

if analysisType == 3:
    print "Running unbinned likelihood analysis"
    print "Two likelihood fits will be done, SED will be created by pytools"

    if runPreAna:
        preAnalysisUnbinned()

    unbinnedLike()


