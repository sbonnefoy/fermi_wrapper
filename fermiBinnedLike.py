import pyLikelihood
from BinnedAnalysis import *
from readLikeXML import *
from bdlikeSED import *
from math import log10
import sys


class fermiBinnedAnalysis:
    """ Class computing the binned likelihood analysis of Fermi data"""
  

    def __init__(self, src, srcFermi, scFile, gtiFile, ltcubeFile, srcmapsFile, expcubeFile, ccubeFile, irfs, EBIN, EMIN, EMAX, model, tolerance):

        self.src=src
        self.srcFermi = srcFermi
        self.scFile=scFile
        self.gtiFile=gtiFile
        self.ltcubeFile=ltcubeFile
        self.srcmapsFile=srcmapsFile
        self.expcubeFile=expcubeFile
        self.ccubeFile=ccubeFile
        self.irfs=irfs
        self.EBIN=EBIN
        self.EMIN=EMIN
        self.EMAX=EMAX
        self.model=model
        self.tolerance=tolerance

        global obs 
        obs = BinnedObs(self.srcmapsFile, self.ltcubeFile, self.expcubeFile, irfs=self.irfs)



    def makeBin(self):
        low_edges=[]
        high_edges=[]
        energyLow=float(self.EMIN)
        incremEnergy=(log10(self.EMAX)-log10(self.EMIN))/self.EBIN
        while energyLow < float(self.EMAX):
            energyUp = energyLow*(10**(incremEnergy))
            low_edges.append(float(energyLow))
            high_edges.append(float(energyUp))
            energyLow=energyUp
 

        return (low_edges, high_edges)

    
    def likelihood1(self, optmz, emin, emax, plot, write, analysisType, specType):
        """Computing a first likelihood fit."""

        global obs 
        obs = BinnedObs(self.srcmapsFile, self.ltcubeFile, self.expcubeFile, irfs=self.irfs)
        global like1
        
      


        #Running the fit a first time with MINUIT minimizer
        print "Fitting the SED between ", emin ," and ", emax, " MeV"
        like1 = BinnedAnalysis(obs,self.model, 'MINUIT')
        print obs
        print like1
        like1.tol = self.tolerance
        like1.setEnergyRange(emin, emax)
        like1.fit(verbosity=1)
        likeobj = pyLike.Minuit(like1.logLike)
        print "RetCode. It should be 0 -> ", likeobj.getRetCode()
        ts = like1.Ts(self.srcFermi)
        print like1.model[self.srcFermi]
        print "Computed TS value: ", ts
        print "Computed loglikelihood value: ", like1.logLike.value()


        if specType!="PL":
            print "Checking the details of the sources"
            sourceDetails = {}
            for sourceName in like1.sourceNames():
                sourceDetails[sourceName] = like1.Ts(sourceName)
            print sourceDetails


            below25List = []
            # Loop over sources
            for sourceName in like1.sourceNames():
                tsSource = like1.Ts(sourceName)
                
                # Get spectrum object
                # print "Taking care of ", sourceName
                spectrum = like1.model[sourceName].funcs['Spectrum']
                # Loop over spectrum parameters: Prefactor, Index, Scale, ...
                
                if (tsSource < 2):
                    print sourceName, " TS is ", tsSource, " lower than 2, deleting..."
                    like1.deleteSource(sourceName)



                if (tsSource < 25):
                    print sourceName, " TS is ", tsSource, " lower than 5"
                    for parName in spectrum.paramNames:
                        if spectrum.getParam(parName).isFree():
                            below25List.append(sourceName)
                            if (parName == 'Prefactor') or (parName == 'Integral') or (parName == 'norm') or (parName == 'Normalization'):
                                print ' Fixing parameter \'%s: %s\''%(sourceName, parName)
                                spectrum.getParam(parName).setFree(False)
                                
            print "You should fix the following sources to the catalog values and rerun the fit"
            print "They have TS value < 25"
            for lowSource in below25List:
                print lowSource
                            
#
#                if (tsSource > 25):
#                    print sourceName, " TS is ", tsSource, " greater than 5"
#                    for parName in spectrum.paramNames:
#                        if spectrum.getParam(parName).isFree():
#                            if (parName == 'Prefactor') or (parName == 'Integral') or (parName == 'norm') or (parName == 'Normalization'):
#                                print ' Releasing parameter \'%s: %s\''%(sourceName, parName)
#                                spectrum.getParam(parName).setFree(True)
        like1.logLike.writeXml(str(self.src)+"_model_likehoodFit1.xml")
        print "the first is written"

        if (specType != "PL"):
            print "Saving the new model"
            like1.logLike.writeXml(str(self.src)+"_FullRange_DRMNGB.xml")
        #Running the fit one more time with NEWMINUIT optimizer

       
        print "Computing the fit with NEWMINUIT"
        like1 = BinnedAnalysis(obs,str(self.src)+"_model_likehoodFit1.xml",'NEWMINUIT')
        like1.tol = self.tolerance
        like1.setEnergyRange(emin, emax)
        print "Fitting the SED between ", emin ," and ", emax, " MeV"
        likeobj = pyLike.NewMinuit(like1.logLike)
        #likeobj = pyLike.Minuit(like1.logLike)
        like1.fit(verbosity=1, optObject=likeobj)
        like1.logLike.writeXml(str(self.src)+"_model_FinalFit-"+str(emin)+"-"+str(emax)+"MeV.xml")

        if (specType!="PL"):
            like1.logLike.writeXml(str(self.src)+"_FullRange_NEWMINUIT.xml")
        
        print "Computed RetCod value: ", likeobj.getRetCode(), ", it should be 0"
        


        if analysisType == 1 and specType=="PL":
            preFactor = like1.model[self.srcFermi].funcs['Spectrum'].getParam('Prefactor').value()
            scale = like1.model[self.srcFermi].funcs['Spectrum'].getParam('Scale').value()
            alpha = like1.model[self.srcFermi].funcs['Spectrum'].getParam('Index').value()
            error = like1.model[self.srcFermi].funcs['Spectrum'].getParam('Prefactor').error()
            ts = like1.Ts(self.srcFermi)
            print "Computed TS value: ", ts
            print "Computed loglikelihood value: ", like1.logLike.value()
            print like1.model[self.srcFermi]
            return (preFactor, scale, alpha, error, ts)
    

        if analysisType == 1 and specType=="2PL":
            print "test1"
            preFactorSync = like1.model['NebSync'].funcs['Spectrum'].getParam('Prefactor').value()
            scaleSync = like1.model['NebSync'].funcs['Spectrum'].getParam('Scale').value()
            alphaSync = like1.model['NebSync'].funcs['Spectrum'].getParam('Index').value()
            preFactorIC = like1.model['NebIC'].funcs['Spectrum'].getParam('Prefactor').value()
            scaleIC = like1.model['NebIC'].funcs['Spectrum'].getParam('Scale').value()
            alphaIC = like1.model['NebIC'].funcs['Spectrum'].getParam('Index').value()
            print like1.model['NebSync']
            print like1.model['NebIC']
            return (preFactorSync, scaleSync, alphaSync, preFactorIC, scaleIC, alphaIC)


        if analysisType == 1 and specType=="ExpCutOff":
            preFactor = like1.model[self.srcFermi].funcs['Spectrum'].getParam('Prefactor').value()
            preFactorScale = like1.model[self.srcFermi].funcs['Spectrum'].getParam('scale').value()
            Index = like1.model[self.srcFermi].funcs['Spectrum'].getParam('Index').value()
            Scale = like1.model[self.srcFermi].funcs['Spectrum'].getParam('Scale').value()
            Ebreak = like1.model[self.srcFermi].funcs['Spectrum'].getParam('Ebreak').value()
            P1 = like1.model[self.srcFermi].funcs['Spectrum'].getParam('P1').value()
            P2 = like1.model[self.srcFermi].funcs['Spectrum'].getParam('P2').value()
            P3 = like1.model[self.srcFermi].funcs['Spectrum'].getParam('P3').value()
            print like1.model[self.srcFermi]
            return (preFactor, preFactorScale, Index, Scale, Ebreak, P1, P2, P3)
            
      
        if analysisType == 1 and specType=="PLSExpCutOff":
            preFactor = like1.model[self.srcFermi].funcs['Spectrum'].getParam('Prefactor').value()
            Index1 = like1.model[self.srcFermi].funcs['Spectrum'].getParam('Index1').value()
            Cutoff = like1.model[self.srcFermi].funcs['Spectrum'].getParam('Cutoff').value()
            Scale = like1.model[self.srcFermi].funcs['Spectrum'].getParam('Scale').value()
            Index2 = like1.model[self.srcFermi].funcs['Spectrum'].getParam('Index2').value()
            ts = like1.Ts(self.srcFermi)
            print like1.model[self.srcFermi]
            print "Computed TS value: ", ts
            print "Computed loglikelihood value: ", like1.logLike.value()

            return (preFactor, Index1,Cutoff, Scale, Index2)

        if analysisType == 1 and specType=="LogPara":
            norm = like1.model[self.srcFermi].funcs['Spectrum'].getParam('norm').value()
            alpha = like1.model[self.srcFermi].funcs['Spectrum'].getParam('alpha').value()
            beta = like1.model[self.srcFermi].funcs['Spectrum'].getParam('beta').value()
            Eb = like1.model[self.srcFermi].funcs['Spectrum'].getParam('Eb').value()
            ts = like1.Ts(self.srcFermi)
            print like1.model[self.srcFermi]
            print "Computed TS value: ", ts
            print "Computed loglikelihood value: ", like1.logLike.value()
            
            return (norm, alpha, beta, Eb)


    def likelihood2(self, optmz, write):
        """ Computing a second likelihood fit needed to compute the SED"""
       
        global like2 
        like2 = BinnedAnalysis(obs,str(self.src)+"_model_likehoodFit1.xml" ,optimizer=optmz)
        like2.tol = 0.001
        like2obj = pyLike.NewMinuit(like2.logLike)
        like2.fit(verbosity=1,covar=True,optObject=like2obj)

        if write:
            like2.logLike.writeXml(str(self.src)+"_model_likehoodFit2.xml")

        

    def SED(self):
        """Computing the custom bin and the final SED"""

        if self.EBIN==9:
            print "Working on 9 energy bins"
            low_edges = [200.,427.69,914.61,1955.87,4182.56,8944.27,19127.05,40902.61]
            high_edges = [427.69,914.61,1955.87,4182.56,8944.27,19127.05,40902.61,187049.69]
            
        else:
            print "Working on ", self.EBIN, " energy bins"
            low_edges=[]
            high_edges=[]
            low_edges, high_edges = self.makeBin()

        print "Energy bins created: ", low_edges, high_edges
        inputs = bdlikeInput(like2,self.gtiFile,self.ccubeFile, self.scFile, \
                                 self.srcFermi, model=str(self.src)+"_model_likehoodFit2.xml", nbins=self.EBIN)
        
        print "input object created"
        inputs.customBins(low_edges,high_edges)
        print "custom bin done"
        inputs.fullFit(CoVar=True)
        print "full fit done"
        sed = bdlikeSED(inputs)
        sed.getECent()
        sed.fitBands()
        sed.Plot()
        
