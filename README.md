==============================================================================    
PLEASE, NOTE THAT IS VERSION MIGHT BE OUTDATED AND MIGHT NOT WORK WITH    
THE CURRENT VERSION OF THE FERMI TOOLS.

This code was used for scientific publication in the journal   
Astronomy and Astrophysics (A&A 591, A138 (2016))      
https://www.aanda.org/articles/aa/full_html/2016/07/aa27722-15/aa27722-15.html

==============================================================================    

This code was built to run the Fermi tools.  
It produces high-level analysis (spectrum) directly from 
the data downloaded from the Fermi-LAT server.   

It is made to also run pulsar analysis, applying cuts on 
the pulsar period.  

In order to use them, you need a working version of the Fermi tools
on you computer     
(https://fermi.gsfc.nasa.gov/ssc/data/analysis/software/).   
Also ROOT (https://root.cern.ch/) needs to be installed.

Then, you need to set the parameters in the input.py file.
You need to set there:   
                - source name   
                - source coordinates   
                - analysis mode   
                - catalog file   
                - diffuse background file    
                - isotropic model file    

Once the input file is set, you can launch your analysis
by issuing the following command:
`$ python fermiAnalysis.py`


