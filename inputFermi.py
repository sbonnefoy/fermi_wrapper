##############################
#
#  Input of the parameters
#
##############################

######## Analysis mode ############

analysisType=1                        # Set the analysis type you want:
                                      #
                                      # 1 -> Binned likelihood, with power law, spectral index=2 fit in each energy band
                                      # 2 -> Binned likelihood, Minuit + NewMinuit fit, SED using pytools
                                      # 3 -> Unbinned likelihood

runPreAna = 1                          # Set to 1 if you need to (re)create the gtselect, gtmktime... files
fitAlpha=1                            # Fit the power law index, for mode 1 only
pulsarMode=0                          # Set to 1 if you want to run a pulsar analysis; phased events
sedType="TeV"                        # Give the sed as Tev.s^-1.cm^-1
#sedType="ergs"                        # Give the sed as ergs.s^-1.cm^-1
scaleSED=0                            # Should the SED be divided by the phase region (for pulsar mode)
#phaseSoft = "gtpphase"                 #specify which software was used for the events phasing (pulsar mode only)
phaseSoft = "tempo2"

######## Source parameters ########
srcName="J0614"                            # Source name
srcNameFermi="3FGL J0614.1-3329"           # Srouce name in the fermi catalog
PSRNAME=""                          # If you run pulsar analysis, the name of the pulsar in the ephemeris database
RA=93.5368
DEC=-33.4949

# specify the spectral type of the source: PL, 2PL, ExpCutOff, LogPara, PLSExpCutOff
specType="PLSExpCutOff"

####### Analysis parameters ######
ROI=20                                  # Region of interest
EMIN=10000                                # Energy min
EMAX=500000                             # Energy max
EBIN=4                                 # Bin in energy
ZMAX=90                                # Zenithal angle cut, usually 100 as suggested by Fermi
PHASEMIN='0.0'                        # Phase min, for pulsar analysis
PHASEMAX='1.0'                        # Phase max, for pulsar analysis
solarEph="JPL DE405"
factorLimit=1e-14                      # Limit on the prefactor of the sources taken into account for the model
tsLimit=5                             # TS value below wich point won't be plotted in mode 1
tol= 1e-8                                 # Tolerance of the fit
optimizer='NEWMINUIT'
phaseCut='(PULSE_PHASE >= 0.00 && PULSE_PHASE <= 1.0)' 
phaseFactor=1.0    # Duty cycle of the pulsar this number has to be a float
                   # If not pulsar analysis, set to 1.0



######### Files input #########

evfile="@events.txt"
scfile="L171115094054E8D3AA3F99_SC00.fits"
EPHEMERIS='geminga_till2014.fits'                            # Set 'none' if you are not running pulsar analysis
IRFS='P8R2_SOURCE_V6'

model="J0614-3329.xml"
model1=model
catalog="gll_psc_v16.fit"
galactic="gll_iem_v06.fits"
iso="iso_P8R2_SOURCE_V6_v06.txt"


